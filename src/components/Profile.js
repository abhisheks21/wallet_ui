// Obselete, Now replaced with dashboard.js

import React from "react";
import AuthService from "../services/auth.service";

const Profile = () => {
  const currentUser = AuthService.getCurrentUser();

  return (
    
    <div className="card card-container">
      <header className="jumbotron ml-auto">
        <h3>
          <strong>{currentUser.username}</strong> Wallet
        </h3>
      </header>
      <p>
        <strong>Token:</strong> {currentUser.accessToken.substring(0, 20)} ...{" "}
        {currentUser.accessToken.substr(currentUser.accessToken.length - 20)}
      </p>
      <p>
        <strong>Id:</strong> {currentUser.id}
      </p>
      <p>
        <strong>Email:</strong> {currentUser.email}
      </p>
      <p>
        <strong>Wallet Balance:</strong> {currentUser.walletBalance}
      </p>
      <p>
          <button>Transfer Money</button>
      </p>
      <p>
          <button>View Account Statement</button>
      </p>
      {/* <strong>Authorities:</strong>
      <ul>
        {currentUser.roles &&
          currentUser.roles.map((role, index) => <li key={index}>{role}</li>)}
      </ul> */}
    </div>
  );
};

export default Profile;