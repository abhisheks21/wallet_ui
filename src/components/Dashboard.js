import React, { useRef, useState } from "react";
import Form from "react-validation/build/form";
import Input from "react-validation/build/input";

import AuthService from "../services/auth.service";
import UserService from "../services/user.service";
import Swal from "sweetalert2";

function isPositiveNumeric(n) {
  return !isNaN(parseInt(n)) && isFinite(n) && (parseInt(n)>0);
}

const required = (value) => {
    if (!value) {
      return (
        <div className="alert alert-danger" role="alert">
          This field is required!
        </div>
      );
    }
  };

const onlyDigits = (value) => {
  if(!isPositiveNumeric(value)){
      return (
        <div className="alert alert-danger" role="alert">
          Invalid amount!
        </div>
      );
  }
};

const Dashboard = () => {
  const currentUser = AuthService.getCurrentUser();

  const rechargeForm = useRef();
  const transferForm = useRef();

  const [balance, setBalance] = useState(currentUser.walletBalance);
  const [transactions, setTransactions] = useState("");
  const [loading, setLoading] = useState(false);
  const [rechargeAmount, setRechargeAmount] = useState(0);
  const [showRechargeForm, setShowRechargeForm] = useState(false);
  const [transferAmount, setTransferAmount] = useState(0);
  const [transferId, setTransferId] = useState("");
  const [showTransferForm, setShowTransferForm] = useState(false);
  const [showStatement, setShowStatement] = useState(false);

  window.onload = setInterval(() => {
    UserService.updatedBalance().then(
      (response) => {
        // setBalance(response.data)
        console.log(response.data.message,"\n")
        setBalance(response.data.message)
      },(error) => {
        console.log(error)
      }
    )
  }, 6000)

  const onChangeRechargeAmount = (e) => {
      setRechargeAmount(e.target.value)
  }

  const onChangeTransferAmount = (e) => {
      setTransferAmount(e.target.value)
  }

  const onChangeTransferId = (e) => {
      setTransferId(e.target.value)
  }

  const handleRecharge = (e) => {
    e.preventDefault();

    setLoading(true);

    UserService.rechargeAccount(rechargeAmount).then(
        (response) => {
            console.log(response.data);
            setBalance(response.data.accountBalance);
            setShowRechargeForm(false);
            setLoading(false);
            Swal.fire({
              icon: 'success',
              title: "Recharge",
              text: "Recharge Successful!"
            })
            // window.alert("Recharge successful!")

        },
        (error) => {
            const _content =  (error.response &&
                        error.response.data &&
                        error.response.data.message) ||
                        error.message || error.toString();
            if(error && error.response && error.response.status && error.response.status===401){
              Swal.fire({
                icon: 'error',
                title: "Error",
                text: "Credentials invalid. Please login again!"
              })
              // window.alert("Credentials invalid. Please login again!");
              console.log(_content)
            }
            setLoading(false);

        }
    );
  }

  const handleTransfer = (e) => {
    e.preventDefault();

    setLoading(true);

    UserService.transferToAccount(transferId, transferAmount).then(
        (response) => {
            console.log(response.data);
            setBalance(response.data.accountBalance);
            setShowTransferForm(false)
            setLoading(false);
            Swal.fire({
              icon: 'success',
              title: "Tranfer",
              text: "Transaction Successful!"
            })
            // window.alert("Amount transferred!")
        },
        (error) => {
            const _content =  (error.response &&
                        error.response.data &&
                        error.response.data.message) ||
                        error.message || error.toString();
            if(error && error.response && error.response.data &&
               error.response.data.message &&
               error.response.data.message.message){
                window.alert(error.response.data.message.message)
               }
            console.log(error.response);
            setLoading(false);
        }
    );
  }

  const displayStatement = (e) => {
      e.preventDefault();
      setShowStatement(true);

      UserService.accountStatement().then(
        (response) => {
            console.log(response.data);
            setTransactions(response.data.transactions)
            setLoading(false);
        },
        (error) => {
            const _content =  (error.response &&
                        error.response.data &&
                        error.response.data.message) ||
                        error.message || error.toString();
            if(error && error.response && error.response.status && error.response.status===401){
              Swal.fire({
                icon: 'error',
                title: "Error",
                text: "Credentials invalid. Please login again!"
              })
              // window.alert("Credentials invalid. Please login again!");
              console.log(_content)
            }
            // console.log(error.response);
            setLoading(false);
        }
      );
  }


  return (
    <div className="container d-flex"> 
    <div className="row justify-content-center usercard">   
    {/* <div className="col"> */}
     <div className="card">
      <header className="jumbotron ml-auto">
        <h3>
          <strong>{currentUser.username}'s</strong> Wallet
        </h3>
      </header>
      {/* <p>
        <strong>Token:</strong> {currentUser.accessToken.substring(0, 20)} ...{" "}
        {currentUser.accessToken.substr(currentUser.accessToken.length - 20)}
      </p> */}
      <p>
        <strong>Id:</strong> {currentUser.id}
      </p>
      <p>
        <strong>Email:</strong> {currentUser.email}
      </p>
      <p>
        <strong>Wallet Balance: ₹</strong> {balance}
      </p>
      <p>
          <button onClick={() => setShowRechargeForm(true)}>Add Money</button>
          {showRechargeForm && (
              <Form onSubmit={handleRecharge} ref={rechargeForm}>
                  <div className="form-group">
                    <label htmlFor="rechargeAmount">Recharge Amount (in ₹)</label>
                    <Input 
                        type="text"
                        className="form-control"
                        name="rechargeAmount"
                        value={rechargeAmount}  
                        onChange={onChangeRechargeAmount}  
                        validations={[required, onlyDigits]}
                    />
                  </div>

                  <div className="form-group">
                    <button className="btn btn-primary btn-block" disabled={loading}>
                        {loading && (
                        <span className="spinner-border spinner-border-sm"></span>
                        )}
                        <span>Recharge!</span>
                    </button>
                  </div>
              </Form>
          )}
      </p>
      <p>
          <button onClick={() => setShowTransferForm(true)}>Transfer Money</button>
          {showTransferForm && (
              <Form onSubmit={handleTransfer} ref={transferForm}>
                  <div className="form-group">
                    <label htmlFor="transferId">Receiver ID</label>
                    <Input 
                        type="text"
                        className="form-control"
                        name="transferId"
                        value={transferId}  
                        onChange={onChangeTransferId}  
                        validations={[required]}
                    />
                  </div>
                  <div className="form-group">
                    <label htmlFor="transferAmount">Transfer Amount (in ₹)</label>
                    <Input 
                        type="text"
                        className="form-control"
                        name="transferAmount"
                        value={transferAmount}  
                        onChange={onChangeTransferAmount}  
                        validations={[required, onlyDigits]}
                    />
                  </div>
                  <div className="form-group">
                    <button className="btn btn-primary btn-block" disabled={loading}>
                        {loading && (
                        <span className="spinner-border spinner-border-sm"></span>
                        )}
                        <span>Transfer!</span>
                    </button>
                  </div>
              </Form>
          )}
      </p>
      <div>
          <button onClick={displayStatement}>View Account Statement</button>
      </div>
      {/* <strong>Authorities:</strong>
      <ul>
        {currentUser.roles &&
          currentUser.roles.map((role, index) => <li key={index}>{role}</li>)}
      </ul> */}
    {/* </div> */}
    </div>
    <div className="row" style={{display: (showStatement? "block": "none")}}>
        <div className="card-container">
            {showStatement && (
                // <div className="table-responsive">
                <div>
                    <h3>Account Statement</h3>
                <table className="table table-dark table-striped table-sm
                            table-bordered">
                    <thead>
                        <tr className="sticky-top sticky-offset">
                            <th scope="col">Time</th>
                            <th scope="col">Id</th>
                            <th scope="col">Sender</th>
                            <th scope="col">Receiver</th>
                            <th scope="col">Amount (₹)</th>
                        </tr>
                    </thead>
                    <tbody>
                        {Array.isArray(transactions)? (transactions.map((t) => {
                            return (
                                <tr>
                                    <td>{t.timestamp.substring(0,10)} {t.timestamp.substring(11,16)}</td>
                                    <td>{t.transactionId}</td>
                                    <td>{t.senderId}</td>
                                    <td>{t.receiverId}</td>
                                    <td>{t.amount}</td>
                                </tr>
                            );
                        })):<div></div>}
                    </tbody>
                </table>
                </div>
            )}
        </div>
    </div>
    </div>
    </div>
  );
};

export default Dashboard;

//  let config = {
//     headers: { 'Access-Control-Allow-Origin': "*"}
// }