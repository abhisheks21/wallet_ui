import axios from "axios";
import authHeader from "./auth-header";

const API_URL_TEST = "http://localhost:8091/api/test/";
const API_URL_TRANSACT = "http://localhost:8091/api/transaction/";

const getPublicContent = () => {
  return axios.get(API_URL_TEST + "all");
};

const getUserBoard = () => {
  return axios.get(API_URL_TEST + "user", { headers: authHeader() });
};

const getModeratorBoard = () => {
  return axios.get(API_URL_TEST + "mod", { headers: authHeader() });
};

const getAdminBoard = () => {
  return axios.get(API_URL_TEST + "admin", { headers: authHeader() });
};

const rechargeAccount = (amount) => {
  return axios.patch(API_URL_TRANSACT + "topup", {}, { headers: authHeader(), params: { amount } })
};

const transferToAccount = (receiverId, transferAmount) => {
  return axios.post(API_URL_TRANSACT + "transfer", {}, { 
                              headers: authHeader(), 
                              params: { transferAmount, receiverId }
                            });
}

const accountStatement = () => {
  return axios.get(API_URL_TRANSACT + "accountStatement", { headers: authHeader() });
}

const updatedBalance = () => {
  return axios.get(API_URL_TRANSACT + "accountBalance", { headers: authHeader() });
}


export default {
  getPublicContent,
  getUserBoard,
  getModeratorBoard,
  getAdminBoard,
  rechargeAccount,
  transferToAccount,
  accountStatement,
  updatedBalance
};